var gulp = require('gulp');
var babel = require('gulp-babel');
var replace = require('gulp-replace');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var gcmq = require('gulp-group-css-media-queries');
var sassGlob = require('gulp-sass-glob');
//var csslint = require('gulp-csslint');
var jsSource, scssSource;

scssSource = ['scss/template.scss','scss/*/*.scss','scss/*/*/*.scss','scss/*.scss'];
jsSource = ['js/template.js','js/*.js','js/*/*.js','js/*/*/*.js'];
var user=process.argv[3];
if(user==undefined){
    user="global";
}
console.log("");
console.log("");
console.log("##############################################");
console.log('PLIKI BEDA GENEROWANE W KATALOGU : build/'+user);
console.log("##############################################");
gulp.task('scss', function() {
    var versionDate = new Date();
    return gulp.src("scss/template.scss")
        .pipe(sassGlob())
        .on('error', swallowError)
        .pipe(sass())
        .on('error', swallowError)
        .pipe(replace('{date}', versionDate))
        .on('error', swallowError)
        .pipe(autoprefixer({ browsers: ['>0%']}))
        .on('error', swallowError)
        .pipe(gcmq())
//	.pipe(csslint())
//	.pipe(csslint.formatter()) 
        .on('error', swallowError)
        .pipe(gulp.dest('build/'+user+'/css'))
});

gulp.task('js', function() {
    return gulp.src(jsSource)
        .pipe(concat('template.js'))
        .on('error', swallowError)
        .pipe(babel())
        .on('error', swallowError)
        .pipe(gulp.dest('build/'+user+'/js'))
});

gulp.task('default', ['scss','js'], function() {
    gulp.watch(scssSource, ['scss']);
    gulp.watch(jsSource, ['js']);
});

function swallowError (error) {

  // If you want details of the error in the console
  console.log(error.toString())

  this.emit('end')
}
